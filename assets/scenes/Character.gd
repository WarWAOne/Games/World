extends KinematicBody

# Movements
var direction = Vector3()
var velocity = Vector3()

# Fly
var FLY_SPEED = 30 # Walking speed
var FLY_ACCEL = 3 # Walking acceleration

# Walk
var gravity = -9.8 * 3
const MAX_SPEED = 20
const MAX_RUNNING_SPEED = 30
const ACCEL = 2
const DEACCEL = 6

# Jump
var jump_height = 15
var in_air = 0
var has_contact = false

# Slope
const MAX_SLOPE_ANGLE = 35

# Stair
const MAX_STAIR_SLOPE = 20
const STAIR_JUMP_HEIGHT = 6

# Camera
var camera_angle = 0
var mouse_sensitivity =  0.3
	
func _input(event):
	# Camera movement with mouse
	if event is InputEventMouseMotion:
		# Allow turning head and camera in the x
		$Head.rotate_y(deg2rad(-event.relative.x * mouse_sensitivity))
		# Allow turning head and camera in the y
		var change = -event.relative.y * mouse_sensitivity
		# Stop camera when angle more than 90
		if change + camera_angle < 90 and change + camera_angle > -90:
			$Head/Camera.rotate_x(deg2rad(change))
			camera_angle += change
	
	
func _physics_process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
	walk(delta)
	pass

func fly(delta):
	# Reset the direction of the player
	direction = Vector3() 

	# Get the rotation of the camera
	var aim = $Head/Camera.get_global_transform().basis

	# Check inputs and change direction
	if Input.is_action_pressed("ui_right"):
		direction += aim.x
	if Input.is_action_pressed("ui_left"):
		direction -= aim.x
	if Input.is_action_pressed("ui_down"):
		direction += aim.z
	if Input.is_action_pressed("ui_up"):
		direction -= aim.z
	
	direction = direction.normalized()
	var target = direction * FLY_SPEED 
	velocity = velocity.linear_interpolate(target, FLY_ACCEL * delta)
	
	# Move the player
	move_and_slide(velocity)

func walk(delta):
	# Reset the direction of the player
	direction = Vector3() 

	# Get the rotation of the camera
	var aim = $Head/Camera.get_global_transform().basis

	# Check inputs and change direction
	if Input.is_action_pressed("ui_right"):
		direction += aim.x
	if Input.is_action_pressed("ui_left"):
		direction -= aim.x
	if Input.is_action_pressed("ui_down"):
		direction += aim.z
	if Input.is_action_pressed("ui_up"):
		direction -= aim.z
	
	direction = direction.normalized()
	
	velocity.y += gravity * delta
	var temp_velocity = velocity
	temp_velocity.y = 0
	
	# Sprint
	var speed
	if Input.is_action_pressed("move_sprint"):
		speed = MAX_RUNNING_SPEED
	else: 
		speed = MAX_SPEED
	
	# Where would the player go at max speed
	var target = direction * speed 
	
	var acceleration
	if direction.dot(temp_velocity) > 0:
		acceleration = ACCEL
	else:
		acceleration = DEACCEL
	
	temp_velocity = temp_velocity.linear_interpolate(target, acceleration * delta)
	velocity.z = temp_velocity.z
	
	# Move the player
	velocity = move_and_slide(velocity, Vector3(0, 1, 0))
	
	# Jump
	if Input.is_action_just_pressed("jump"):
		velocity.y = jump_height